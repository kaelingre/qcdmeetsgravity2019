<!doctype html>

<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=1024" />
    <title>Boundary Data Meets Bound States -- QCD Meets Gravity 2019, UCLA</title>
    
    <meta name="description" content="Presentation Slides made with impress.js" />
    <meta name="author" content="Gregor Kaelin" />

	<script type="text/x-mathjax-config">
	  MathJax.Hub.Config({
      "HTML-CSS": {
	  scale: 90,
	  styles: {
      '.MathJax_Display': {
      "margin": "20px 0"
      }
      }
	  },
	  TeX: { extensions: ["color.js"] },
	  });
	</script>
	<script type="text/javascript" src="extras/mathjax/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>

    <link href="css/main.css" rel="stylesheet" />
    <link rel="shortcut icon" href="favicon.png" />
    <link rel="apple-touch-icon" href="apple-touch-icon.png" />
  </head>

  <body class="impress-not-supported">

	<div class="fallback-message">
      <p>Your browser <b>doesn't support the features required</b> by impress.js, so you are presented with a simplified version of this presentation.</p>
      <p>For the best experience please use the latest <b>Chrome</b>, <b>Safari</b> or <b>Firefox</b> browser.</p>
	  <p>
		$$
		\DeclareMathOperator{\Arcsinh}{arcsinh}
		\DeclareMathOperator{\Arctan}{arctan}
		\DeclareMathOperator{\arcosh}{arcosh}
		\newcommand\dd{{\mathrm d}}
		\newcommand{\bb}{{\mathbf b}}
		\newcommand{\bk}{{\mathbf k}}
		\newcommand{\bl}{{\mathbf l}}
		\newcommand{\bm}{{\mathbf m}}
		\newcommand{\bn}{{\mathbf n}}
		\newcommand{\bp}{{\mathbf p}}
		\newcommand{\bq}{{\mathbf q}}
		\newcommand{\br}{{\mathbf r}}
		\newcommand{\bw}{{\mathbf w}}
		\newcommand{\bx}{{\mathbf x}}
		\newcommand{\by}{{\mathbf y}}
		\newcommand{\bz}{{\mathbf z}}
		\newcommand\cM{\mathcal{M}}
		\newcommand\cO{\mathcal{O}}
		\newcommand\cE{\mathcal{E}}
		\newcommand\cS{\mathcal{S}}
		\newcommand\cP{\mathcal{P}}
		\newcommand\cF{\mathcal{F}}
		$$
	  </p>
	</div>

	<div id="impress">

	  <div id="title" class="step" data-x="0" data-y="0">
		<h1>Boundary Data Meets Bound States </h1>
		<p class="margin-bottom30">Work with Rafael Porto in<br/>
		  <a href="http://arxiv.org/abs/arXiv:1910.03008">[1910.03008]</a>&
		  <a href="http://arxiv.org/abs/arXiv:1911.09130">[1911.09130]</a>
		</p>
		
		<img id="scatteringFig" src="figures/scattering.svg" alt="scattering">
		<img id="orbitFig" src="figures/orbit.svg" alt="orbit">
		<p id="author">
		  Gregor Kälin
		</p>
		<img id="slacFig" class="margin-bottom80" src="figures/SLAC.png" alt="slac">
		<p>
		  <em>
			QCD meets Gravity 2019<br/>
			UCLA
		  </em>
		</p>
	  </div>

	  
	  <div id="main" class="step" data-x="1700" data-y="0" >
		<img id="mainFig" class="margin-bottom150 vanishPast" src="figures/main.svg" alt="main">
		<img id="angleFig" src="figures/angle.svg" alt="angle">
		<img id="ampFig" src="figures/amp.svg" alt="amp">
		<p id="disclaimer" class="small vanishPast">
		  Disclaimer: Conservative motion<br/>
		  & non-spinning (for most of the talk)
		</p>
	  </div>

	  <div id="impetus1" class="step" data-x="1700" data-y="0">
		<h2 class="margin-bottom80">meets</h2>
		<div id="impetusEq" class="boxed">
		  <a class="ref small" href="http://arxiv.org/abs/arXiv:1910.09366">[see also Bjerrum-Bohr, Cristofoli, Damgaard 19]</a>
		  <h3>Impetus formula</h3>
		  <hr/>
		  <p> $$\mathbf{p}^2(r,E) = p_\infty^2(E) + \widetilde{\cM}(r,E)$$</p>
		</div>
		<p class="vanishPast margin-bottom80 center">
		  $${\widetilde{\cM}}(r,E) \equiv \frac{1}{2E}\int \frac{\dd^3\bq}{(2\pi)^3}\, {\cal M}(\bq,\bp^2=p_\infty^2(E)) e^{-i\bq\cdot \br}$$
		  IR-finite classical part of the scattering amplitude
		</p>		
	  </div>

	  <div id="impetus2" class="step" data-x="1700" data-y="300">
		<p class="center">
		  <em>Sketch of proof:</em> Match to effective potential scattering
		  $$H_{\rm eff}|\psi_\bp(p_\infty)\rangle = \left(\bp^2 + V_{\rm eff}\right) |\psi_\bp(p_\infty)\rangle= p_\infty^2(E) |\psi_\bp(p_\infty)\rangle\,$$
		  with
		  $$V_{\rm eff} =   -\sum_i P_i(E) \frac{G^i}{r^i}\quad \textrm{(PM expansion)}$$
		</p> 
	  </div>

	  <div id="impetus3" class="step" data-x="1700" data-y="850">
		<p class="center">
		  Iterate Lippman-Schwinger equation:
		  $$\begin{align*}
		  \frac{4\pi}{\rm Vol} \tilde f(\bp^2,\bq)  &=-  \langle \bp+\bq| V_{\rm eff} |\bp\rangle + \dots\\
		  &= \frac{1}{\rm Vol} \sum_i \int \dd^3\br \left( P_i(E)\frac{G^i}{r^i}\right) e^{i\bq\cdot \br}+\dots
		  \end{align*}$$
		  Higher iterations can be shown to be IR-divergent (superclassical). Match cross-section
		  $$\frac{\dd \sigma}{\dd\Omega}=|\tilde{f}|^2=|\cM|^2$$
		  \(\Rightarrow\) Born approximation is exact in the classical limit.<br/>
		  Use iterations to match/subtract IR-divergent terms from the amplitude.
		</p>
		<hr/>
	  </div>

	  <div id="impetus3.5" class="step" data-x="1700" data-y="1430">
		<p class="center">
		  More physically: Follow <a href="https://doi.org/10.1007/JHEP02(2019)137">[Kosower, Maybee, O'Connell 19]</a> to split the
		  <em>classical, localized and instantaneous</em> scattered momentum into:
		  $$\bp_{\rm sc}^2(r,p_\infty^2) \sim \psi^\dagger _\bp(\br,p_\infty)\, (-\nabla^2 - p_\infty^2) \psi_\bp(\br,p_\infty)  =  I_{(1)}(r,E) + I_{(2)}(r,E)$$
		  \(I_1\): linear in amplitude, conservative potential</br>
		  \(I_2\): square in amplitude, radiation reaction (+conservative)
		</p>
		<hr/>
	  </div>

	  <div id="impetus4" class="step" data-x="1700" data-y="1800">
		<p class="center">
		  In PM expanded form:
		  $$\widetilde{\cal M}_n(E) = P_n(E)$$
		  using
		  $$\widetilde{\cal M}(r,E) = \sum_{n=1}^\infty \widetilde{\cal M}_n(E) \left(\frac{G}{r}\right)^n,\quad\bp^2(r,E) =  p_\infty^2(E) +\sum_{n=1}^\infty P_n(E) \left(\frac{G}{r}\right)^n$$
		</p>
		<hr/>
	  </div>

	  <div id="chiFromM" class="step" data-x="1700" data-y="2340">
		<div class="boxed margin-bottom30">
		  $$\chi(b,E) +\pi %= 2b \int_{r_{\rm min}}^\infty \frac{\dd r}{r\sqrt{r^2\bar\bp^2(r,E)-b^2}}\\
		  = 2b \int_{r_{\rm min}}^\infty \frac{\dd r}{r\sqrt{r^2 (1+\cM(r,E)/p_\infty^2)-b^2}}$$
		  <p class="center small">
			\(r_\textrm{min}\) is the positive real root of \(p_r\): \(p_r^2(r,E)=\bp^2(r,E)-J^2/r^2\) \(\,\,(J=p_\infty b)\)
		  </p>
		</div>
		<p class="center">
		  Or PM expanded (using \(f_n (E) = \frac{\cM_n(E)}{p_\infty^2(E)M^n}\) ):
		  $$\chi_b^{(n)} = \frac{\sqrt{\pi}}{2} \Gamma\left(\frac{n+1}{2}\right)\sum_{\sigma\in\mathcal{P}(n)}\frac{1}{\Gamma\left(1+\frac{n}{2} -\Sigma^\ell\right)}\prod_{\ell} \frac{f_{\sigma_{\ell}}^{\sigma^{\ell}}}{\sigma^{\ell}!}$$
		  with
		  $$\frac{1}{2} \chi(b,E) =  \sum_n \chi^{(n)}_b (E)  \left(\frac{GM}{b}\right)^n =\sum_n \chi^{(n)}_{j} (E)\frac{1}{j^n}$$
		</p>
		<hr/>
	  </div>
	  
	  <div id="firsov" class="step" data-x="1700" data-y="3050">
		<div class="boxed margin-bottom30">
		  <a class="ref" href="">[Firsov 53]</a>
		  <h3>Firsov's inversion formula</h3>
		  $$\bar\bp^2(r,E) = \exp\left[ \frac{2}{\pi} \int_{r|\bar{\bp}(r,E)|}^\infty \frac{\chi(\tilde b,E)\dd\tilde b}{\sqrt{\tilde b^2-r^2\bar\bp^2(r,E)}}\right]$$
		</div>
		<div class="boxed">
		  $$\frac{1}{2E\,p_\infty^2} \int \frac{\dd^3\bq}{(2\pi)^3} {\cal M}(\bq,\bp)e^{-i\bq\cdot\br_{\rm min}(b,E)} = \exp\left[ \frac{2}{\pi} \int_{b}^\infty \frac{\chi(\tilde b,E)\dd\tilde b}{\sqrt{\tilde b^2-b^2}}\right]-1$$
		</div>
		<p>
		  $$f_n = \sum_{\sigma\in\mathcal{P}(n)}\frac{2(2-n)^{\Sigma^{\ell} - 1}}{\prod_{\ell} (2\sigma^{\ell})!!} \prod_{\ell} \left(\frac{2}{\sqrt{\pi}}\frac{\Gamma(\frac{\sigma_\ell}{2})}{\Gamma(\frac{\sigma_\ell+1}{2})}\chi^{(\sigma_\ell)}_b\right)^{\sigma^{\ell}}$$
		</p>
	  </div>

	  <div id="main2" class="step" data-x="3400" data-y="0" >
		<img id="mainFig" class="margin-bottom150 vanishPast" src="figures/main.svg" alt="main">
		<img id="ampFig" src="figures/amp.svg" alt="amp">
		<img id="srFig" src="figures/sr.svg" alt="sr">
	  </div>

	  <div id="radialAction" class="step" data-x="3400" data-y="0">
		<h2 class="margin-bottom120">meets</h2>
		<div class="refParent">
		  <a class="ref" href="https://doi.org/10.1103/PhysRevD.62.044024">
			[Damour, Jaranowski, Schäfer 00]
		  </a>
		</div>
		<div class="boxed margin-bottom80">
		  <h3>Radial action via analytic continuation \({\cE} = \frac{E-M}{\mu} < 0\)</h3>
		  <a id="extraRef" class="ref" href="http://arxiv.org/abs/arXiv:1910.03008">
			[GK, Porto 19]
		  </a>
		  $$\begin{align*}
		  {\cal S}_r(J,{\cal E}) &\equiv \frac{1}{\pi} \int_{r_-}^{r_+} p_r \dd r = \frac{1}{\pi} \int_{r_-}^{r_+} \sqrt{\bp^2(r,{\cal E})-J^2/r^2} \, \dd r\\
		  &= \frac{1}{\pi} \int_{r_-}^{r_+} \sqrt {p_\infty^2({\cal E}) + \widetilde{\cal M}(r,{\cal E})-J^2/r^2}
		  \end{align*}$$
		</div>
	  </div>

	  <div id="orbitEl1" class="step" data-x="3400" data-y="0" data-rotate-y="90">
		<h1>Orbital Elements</h1>
		<img id="scatteringFig" src="figures/scattering.svg" alt="scattering">
		<p class="center">
		  From Firsovs formula find root(s) of \(p_r\):
		  $$r_{\textrm{min}} = \tilde r_- =  b \exp\left[ -\frac{1}{\pi} \int_{b}^\infty \frac{\chi(\tilde b,E)\dd\tilde b}{\sqrt{\tilde b^2-b^2}}\right]
		  =b \prod_{n=1}^\infty e^{-\frac{(GM)^n\chi_b^{(n)}(\beta)\Gamma\left(\frac{n}{2}\right)}{b^n\sqrt{\pi}\Gamma\left(\frac{n+1}{2}\right)}}$$
		<p>
	  </div>

	  <div id="orbitEl2" class="step" data-x="3400" data-y="800" data-rotate-y="90">
		<img id="orbitFig" src="figures/orbit.svg" alt="orbit">
		<p class="center">
		  Do an analytic continuation to \(J=p_\infty b\) with \(b\in i\mathbb{R}\)  (\(p_\infty^2\leq0\)):
		  $$\begin{align*}
		  r_-(J) &= r_{\textrm{min}}(b)\\
		  r_+(J) &= r_-(-J) = r_{\textrm{min}}(-b)
		  \end{align*}$$
		  These are the two real positive roots of \(p_r\) the radial momentum.
		</p>
	  </div>

	  <div id="orbitEl3" class="step" data-x="3400" data-y="1400" data-rotate-y="90">
		<img id="gif" src="figures/anim.gif" alt="gif">
	  </div>
	  

	  <div id="radialActionPM" class="step" data-x="3400" data-y="550">
		<p class="center">
		  PM expand:
		  $${\cS}_r(J,{\cE}) = \frac{1}{\pi} \int_{r_-}^{r_+} \dd r \sqrt{ Q(J,{\cE},r) + \lambda \sum_{\ell=1}^\infty \frac{D_\ell({\cE})}{r^{\ell+2}}}$$
		  $$\begin{align*}
		  Q(J,{\cal E},r) &\equiv A ({\cal E}) + \frac{2B({\cal E})}{r} + \frac{C(J,{\cal E})}{r^2}\\
A({\cal E}) &\equiv p_\infty^2({\cal E}) & 2B({\cal E}) &\equiv \widetilde{M}_1({\cal E}) G \\
C(J,{\cal E}) &\equiv \widetilde{M}_2({\cal E}) G^2 -J^2 & D_n({\cal E}) &\equiv \widetilde{M}_{n+2} ({\cal E}) G^{n+2}
		  \end{align*}$$
		</p>
		<hr/>
	  </div>

	  <div id="masterInts" class="step" data-x="3400" data-y="1100">
		<p class="center">
		  Master integrals (Hypergeometric function!):
		  $$\begin{align}
		  &\cS_{\{m,q\}} = \frac{1}{2\pi} \oint_C \frac{\dd r}{r^m}Q^{\tfrac{1}{2}-q}\\
		  &= \cS_{\{m,q\}}^\infty
		  \!+\!\sum_{k} \frac{(-1)^q i^{k+m+1}2^k\Gamma\left(\frac{m+k-1}{2}\right)}{\Gamma(k+1)\Gamma\left(\frac{2+m-k-2q}{2}\right)\Gamma\left(q-\frac{1}{2}\right)}\frac{A(\mathcal E)^{\frac{m-k-2q}{2}}B(\mathcal E)^k}{C(J,\mathcal E)^{\frac{m+k-1}{2}}}
		  \end{align}$$
		  Leads to the PM expanded radial action:
		  $$\cS_r =-\sum_{n=0}^\infty \sum_{\sigma\in\cP(n)}
  \frac{(-1)^{ \Sigma^{\ell}}\Gamma\left(\Sigma^{\ell} - \frac{1}{2}\right)}{2\sqrt{\pi}}\cS_{\left\{n+2\Sigma^{\ell},\Sigma^{\ell}\right\}}(J,\cE)\prod_{\ell} \frac{D_{\sigma_{\ell}}^{\sigma^{\ell}}(\cE)}{\sigma^{\ell}!}$$
		</p>
	  </div>

	  <div id="main3" class="step" data-x="5100" data-y="0" >
		<img id="mainFig" class="margin-bottom150 vanishPast" src="figures/main.svg" alt="main">
		<img id="phiFig" src="figures/phi.svg" alt="phi">
		<img id="angleFig" src="figures/angle.svg" alt="angle">
	  </div>

	  <div id="periastron1" class="step" data-x="5100" data-y="0">
		<h2 class="margin-bottom150">meets</h2>
		<p class="center margin-bottom30">
		  Compare the scattering case \(\cE \geq 0\):
		  $$\frac{\chi+\pi}{2\pi}   = -{\partial  \cS_r(J,\cE) \over \partial J} =  \frac{1}{\pi} \int_{{\tilde r}_-(J,\cE)}^\infty \,\, \frac{J}{r^2\sqrt{\bp^2(\cE,r)-J^2/r^2}}\dd r$$
		  to closed orbits \(\cE \leq 0\):
		  $$\frac{\Delta \Phi+2\pi}{2\pi} = - {\partial  \cS_r(J,\cE) \over \partial J} =  \frac{1}{\pi} \int_{r_-(J,\cE)}^{r_+(J,\cE)} \frac{J}{r^2\sqrt{\bp^2(\cE,r)-J^2/r^2}}\dd r$$
		</p>
	  </div>

	  <div id="periastron2" class="step" data-x="5100" data-y="630">
		<div class="boxed">
		  <h3>Periastron advance from Scattering Angle</h3>
		  $$\Delta \Phi(J,\cE<0) = \chi(J,\cE<0) + \chi(-J,\cE<0)$$
		</div>
		<p class="center">
		  Works nicely perturbatively (PM), e.g.:
		  $$\Delta\Phi = \pi \frac{\widetilde\cM_2}{\mu^2M^2 j^2} +\frac{3\pi}{4}\frac{1}{M^4 \mu^4 j^4}\big(\widetilde\cM_2^2+2\widetilde\cM_1\widetilde\cM_3+2p_\infty^2\widetilde\cM_4\big)+\cdots$$
		  \(j = J/(GM \mu)\)
		</p>
	  </div>

	  <div id="main4" class="step" data-x="6800" data-y="0" >
		<img id="mainFig" class="margin-bottom150 vanishPast" src="figures/main.svg" alt="main">
		<img id="phiFig" src="figures/phi.svg" alt="phi">
		<img id="srFig" src="figures/sr.svg" alt="angle">
	  </div>

	  <div id="integrate" class="step" data-x="6800" data-y="0">
		<h2 class="margin-bottom120">meets</h2>
		<p class="center">
		  Having obtained the periastron advance from the scattering angle we can integrate in \(J\) to get \(\cS_r\) (+boundary term at \(\infty\)):
		</p>
		<p class="boxed center margin-bottom120">
		  $$\frac{{\cal S}_r}{GM\mu} =  {\rm sg}(\hat p_\infty
		  )\chi^{(1)}_j(\cE) - j \left(1 + \frac{2}{\pi} \sum_{n=1}  \frac{\chi^{(2n)}_j({\cE})}{(1-2n)j^{2n}}\right)$$
		  \(j = J/(GM \mu)\)
		</p>
	  </div>

	  <div id="srPM" class="step" data-x="6800" data-y="400">
		<p class="center margin-bottom30">
		  Agrees with previously obtained expression in the PM regime!
		  $$\frac{{\cal S}_r}{GM\mu} =  -j +  \frac{{\widetilde \cM}_1}{2 \sqrt{ -\hat p^2_\infty}M \mu^2} + \frac{{\widetilde \cM}_2}{2 jM^2\mu^2} + \frac{{\widetilde \cM}_2^2 +2 {\widetilde \cM}_1{\widetilde \cM}_3+2 p_\infty^2 {\widetilde \cM}_4}{8j^3M^4\mu^4}+\cdots$$
		</p>
	  </div>
	  

	  <div id="main5" class="step" data-x="8500" data-y="0" >
		<img id="mainFig" class="margin-bottom150 vanishPast" src="figures/main.svg" alt="main">
		<img id="obsFig" src="figures/obs.svg" alt="obs">
		<img id="srFig" src="figures/sr.svg" alt="angle">
	  </div>


	  <div id="observables" class="step" data-x="8500" data-y="0">
		<h2 class="margin-bottom120">meets</h2>
		<div class="refParent">
		  <a class="ref" href="https://doi.org/10.1103/PhysRevD.62.044024">
			[Damour, Jaranowski, Schäfer 00]
		  </a>
		</div>
		<ul class="margin-bottom30">
		  <li>Periastron advance:
			\(\frac{\Delta\Phi}{2\pi} = -\frac{\partial}{\partial j} \cS_r(j,\cE)-1\)
		  </li>
		  <li>Periastron-to-periastron period:
			\(\frac{T_p}{2\pi} = \frac{1}{\mu}\frac{\partial}{\partial \cE} \cS_r(j,\cE)\)
		  </li>
		  <li>Radial frequency:
			\(\Omega_r (j,\cE) = \frac{2\pi}{T_p}\)
		  </li>
		  <li>Periastron frequency:
			\(\Omega_p (j,\cE) = \frac{\Delta\Phi}{T_p}\)
		  </li>
		  <a class="ref" href="https://doi.org/10.1103/PhysRevD.92.084021">
			[LeTiec 15]
		  </a>
		  <li>Averaged redshift:
			\(\langle z_a\rangle = 1+\frac{\partial \mu}{\partial m_a} \cE -\Omega_r \frac{\partial}{\partial m_a}\cS_r(j,\cE,m_a) \)
		  </li>
		 </ul>
	  </div>

	  <div id="observablesExpl" class="step" data-x="8500" data-y="500">
		$$\small\begin{aligned}
		\frac{\Delta \Phi_{(L=2)}}{2\pi}&= \frac{3}{4j^2} \frac{5\gamma^2-1}{\Gamma}  +  \frac{27}{32j^4}\frac{(5\gamma^2-1)^2}{\Gamma^2}  -\frac{1}{4 j^4}  \frac{2\gamma^2-1}{\Gamma^2} \Bigg[ 3-54\gamma^2  \\ &+ \nu \left(-6+206 \gamma + 108 \gamma^2+4\gamma^3-\frac{18\Gamma(1-2\gamma^2)(1-5\gamma^2)}{(1+\Gamma)(1+\gamma)} \right)\\
		&\left.- 48 \nu(3+12\gamma^2-4\gamma^4)\frac{\arcsin\sqrt{\frac{1-\gamma}{2}}}{\sqrt{1-\gamma^2}}\right]
		\end{aligned}$$
	  </div>

	  <div id="observablesExpl2" class="step" data-x="8500" data-y="1100">
		$$\small\begin{aligned}
  \frac{GM \Omega^{(L=2)}_r}{\epsilon^{\frac{3}{2}}} &= \textcolor{blue}{1-
   \frac{(15-\nu)}{8}\epsilon+\frac{555+30\nu+11\nu^2}{128 } \epsilon^2} \\
  &+ \left(\textcolor{blue}{\frac{3(2\nu-5)}{2 j}}\textcolor{red}{-\frac{194-184\nu+23\nu^2}{4  j^3}}\right)\epsilon^{\frac{3}{2}}\\
  &+\left(\textcolor{blue}{\frac{15(17-9\nu+2\nu^2)}{8 j}}\textcolor{green}{+\frac{21620-28592\nu+8765\nu^2-865\nu^3}{80  j^3}}\right)\epsilon^{\frac{5}{2}}
  + \cdots  \\
  \frac{G M \Omega^{(L=2)}_\phi}{\epsilon^{\frac{3}{2}}}
  &=\textcolor{blue}{1+\frac{3}{j^2}-\frac{15(2\nu-7)}{4j^4}}+\left(\textcolor{blue}{\frac{1}{8}(\nu-15)+\frac{15(\nu-5)}{8j^2}} \textcolor{red}{-\frac{3(1301-921\nu+102\nu^2)}{32j^4}}\right)\epsilon\\
  &+\left(\textcolor{blue}{\frac{3(2\nu-5)}{2j}}\textcolor{red}{+\frac{-284+220\nu-23\nu^2}{4j^3}}\textcolor{green}{+\frac{3(913-728\nu+106\nu^2)}{j^5}}\right)\epsilon^{\frac{3}{2}}
\\
%  &\left.\textcolor{blue}{+\frac{15(2\nu-7)(194-184\nu+23\nu^2)}{16j^7}}\right)\epsilon^{\frac{3}{2}}
  &+\left(\textcolor{blue}{\frac{1}{128}(555+30\nu+11\nu^2)+\frac{3(895-150\nu+51\nu^2)}{128j^2}}\right.\\
&- \left. \textcolor{green}{\frac{3(-270085+251236\nu-70545\nu^2+7470\nu^3)}{2560j^4}}\right)\epsilon^2\\
  &+\left(\textcolor{blue}{\frac{15(17-9\nu+2\nu^2)}{8j}}
  \textcolor{green}{+\frac{31520-34442\nu+10025\nu^2-865\nu^3}{80j^3}}\right)\epsilon^{\frac{5}{2}}\,.\label{eq:omegas}
%  &\quad +\left.\textcolor{blue}{\frac{3(126665-145879\nu+48850\nu^2-5070\nu^3)}{160j^5}}  % &\quad +\left. \textcolor{blue}{\frac{3(226612-386168\nu+204099\nu^2-40513\nu^3+2788\nu^4)}{64j^7}}  \right)\epsilon^{\frac{5}{2}}+\dots
  \end{aligned}$$
	  </div>

	  <div id="circ" class="step" data-x="10200" data-y="0">
		<h1>Circular Orbit</h1>
		<p class="center">
		  Solve \(r_+(J) = r_-(J)\)
		  $$\Leftrightarrow -2 \sum_{n=0}^\infty \left(\frac{1}{\sqrt{\pi}}\left(\frac{GM}{b}\right)^{2n+1}\frac{\Gamma\left(\frac{2n+1}{2}\right)}{\Gamma(n+1)}\chi_b^{(2n+1)}\right) = i \pi + 2\pi i \mathbb{N}$$
		  to find \(j(\cE) \) and compute the radial frequency:
		  $$GM\Omega_{\rm circ} = \left(\frac{d\, j({\cal E})}{d\, {\cal E}}\right)^{-1}$$
		  Need to resum for a truncated theory \(f_i=0\) for \(i\geq n\)! (More later)
		</p>
	  </div>

	  <div id="circ2" class="step" data-x="10200" data-y="600">
		<p class="center">
		  We can invert to write the binding energy \(\epsilon\equiv -2 \cE\):
		  $$\begin{aligned}
		  \epsilon =&x \left[1 - \frac{x}{12}(9+\nu) - \frac{x^2}{8}\left(27 -19\nu + \frac{\nu^2}{3}\right)\right. \\
		  &+\frac{x^3}{32}\left(\frac{535}{6}-\frac{5585\nu}{6}+135\nu^2-\frac{35\nu^3}{162}\right) \\
&+ \left.\frac{x^4}{384}\left(-10171+\frac{559993}{15}\nu-\frac{34027\nu^2}{3}+\frac{11354\nu^3}{9}+\frac{77\nu^4}{81}\right) + {\cal O}(x^5)\right]
\end{aligned}$$
		  using the standard PN parameter \(x \equiv (GM \Omega_{\rm circ})^{2/3}\).
		</p>
	  </div>

	  <div id="alignSpin" class="step" data-x="11900" data-y="0" >
		<h1>Aligned spins</h1>
		<p class="center">
		  Idea: extend our map to the aligned spins for binary BH problem.<br/>
		  Motion is still in a plane!
		</p>
		<div class="boxed">
		  $$\frac{\chi(J,\cE)+\chi(-J,\cE)}{2\pi} = \frac{\Delta\Phi(J,\cE)}{2\pi}$$
		  <p class="center small">
			where \(J\) is now the <em>total</em> the total angular momentum, i.e. orbital angular momentum + spins.
		  </p>
		</div>
	  </div>

	  <div id="alignSpin2" class="step" data-x="11900" data-y="450">
		<ul>
		  <li>Explicit checks for known PN and PM results work neatly!</li>
		  <li>Relies on the invariance of the (canonical) radial momentum \(p_r\) under \(J\rightarrow -J\), which is true for a quasi-isotropic gauge (given to us automatically by the amplitudes construction).</li>
		  <li>We propose a version of the impetus formula to also hold for the aligned spin case.</li>
		</ul>
	  </div>

	  
	  <div id="resummation" class="step" data-x="13600" data-y="0" >
		<h1>Resummation</h1>
		<p class="center">
		  Let us truncate our theory at given order \(n\), i.e. \(\cM_m=f_m=0\) for \(m \geq n\).<br/>
		  We can try to resum contributions to all orders in \(G\), e.g. for the scattering angle:
		</p>       
        <div class="boxed">
		  $$\begin{align*}
		  \frac{\chi[f_1]}{2} &= \Arctan(y/2)\\
		  \frac{\chi[f_{1,2}]+\pi}{2} &= \frac{1}{\sqrt{1-{\cF}_2 y^2}}\left(\frac{\pi}{2} + \Arctan\left(\frac{y}{2\sqrt{1-{\cF}_2 y^2}}\right)\right)
		  \end{align*}$$
		  <p class="center small">
			with \(y \equiv G M f_1/b\) and \(\cF_2 \equiv f_2/f_1^2\)
		  </p>
        </div>
      </div>

	  <div id="resum2" class="step" data-x="13600" data-y="500">
		<ul>
		  <li> Resummation of \(\Delta\phi\) works similar to \(\chi\).</li>
		  <li> We can resum parts of \(\cS_r\).</li>
		  <li> We can resum \(f_{1,2}\) contributions for \(r_\textrm{min}\) and \(r_\pm\). ("closed form" for real positive roots of arbitrary polynomial?)</li>
		  <li>It becomes hard. More work needs to be done!
		</ul>
	  </div>

	  <div id="conclusions" class="step" data-x="15300" data-y="0" >
		<h1>Conclusions and Outlook</h1>
		<ul class="margin-bottom30">
		  <li>Mapping scattering data to orbital observables does not require a Hamiltonian. Analytic continuation does the job.</li>
		  <li>The (classical) amplitude, the scattering angle, and the momentum contain the same information.</li>
		  <li>Non-perturbative map invites for numerical studies.</li>
		  <li>We can resum. But more work needs to be done!</li>
		  <li>Generalization works for aligned spins.</li>
		  <li>Continue to arbitrary spin? Radiation? Radiation-reaction?</li>
		</ul>
		
	  </div>

	  <div id="final" class="step" data-x="15300" data-y="700">
		<img id="mainFig" src="figures/main.svg" alt="main">
	  </div>

	  <div id="extra" class="step" data-x="15300" data-y="-1500">
		<h2>How would the proposed correction in <a href"http://arxiv.org/abs/arXiv:1912.02139">[Damour 19]</a> change the results?</h2>
		<p class="center">
		  Discrepancy at 6PN! Momentum/amplitude coefficients:
		  $$\delta f_3 = f_3^\textrm{(BCRSSZ)} - f_3^\textrm{(Damour)} = -\frac{1024\nu}{315}\cE^3 + \cO(\cE^4)$$
		  Periastron advance:
		  $$\delta (\Delta\Phi) = \Delta\Phi^\textrm{(BCRSSZ)} - \Delta\Phi^\textrm{(Damour)} = -\frac{1024\nu}{105j^4}\cE^4 + \cO(\cE^5)$$
		  Binding energy for the circular orbits:
		  $$\delta\epsilon= \epsilon^{\textrm{(BCRSSZ)}}-\epsilon^{\textrm{(Damour)}} = \frac{1408 x^7 \nu}{945} +\cO(x^8)$$
	  </div>
	  
	  
  	</div>

	<div id="impress-toolbar"></div>

	<!--
	<div class="hint">
      <p>Use a spacebar or arrow keys to navigate. <br/>
		Press 'P' to launch speaker console.</p>
	</div>
	-->
	<script>
	  if ("ontouchstart" in document.documentElement) { 
		  document.querySelector(".hint").innerHTML = "<p>Swipe left or right to navigate</p>";
	  }
	  </script>

	<script src="js/impress.js"></script>
	<script>impress().init();</script>

  </body>
</html>
